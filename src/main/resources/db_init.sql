-- MySQL Script generated by MySQL Workbench
-- Mon 01 Dec 2014 10:45:10 PM EST
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `safehouses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `safehouses` (
  `id` BINARY(16) NOT NULL,
  `x` DOUBLE NOT NULL,
  `y` DOUBLE NOT NULL,
  `z` DOUBLE NOT NULL,
  `pitch` FLOAT NOT NULL,
  `yaw` FLOAT NOT NULL,
  `world` BINARY(16) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gangs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gangs` (
  `id` BINARY(16) NOT NULL,
  `name` VARCHAR(16) NOT NULL,
  `desc` VARCHAR(255) NULL,
  `kills` INT NOT NULL DEFAULT 0,
  `deaths` INT NOT NULL DEFAULT 0,
  `reputation` DOUBLE NOT NULL,
  `safehouse` BINARY(16) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `fk_gangs_safehouses1_idx` (`safehouse` ASC),
  CONSTRAINT `fk_gangs_safehouses1`
    FOREIGN KEY (`safehouse`)
    REFERENCES `safehouses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `players`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `players` (
  `id` BINARY(16) NOT NULL,
  `name` VARCHAR(16) NOT NULL,
  `displayname` VARCHAR(64) NOT NULL,
  `kills` INT NOT NULL DEFAULT 0,
  `deaths` INT NOT NULL DEFAULT 0,
  `reputation` DOUBLE NOT NULL,
  `role` BINARY(2) NOT NULL DEFAULT 0,
  `gang` BINARY(16) NULL,
  `friendlyFire` TINYINT(1) NULL DEFAULT 0,
  `tagsEnabled` TINYINT(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `displayname_UNIQUE` (`displayname` ASC),
  INDEX `fk_players_gangs_idx` (`gang` ASC),
  CONSTRAINT `fk_players_gangs`
    FOREIGN KEY (`gang`)
    REFERENCES `gangs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gang_invitations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gang_invitations` (
  `player` BINARY(16) NOT NULL,
  `gang` BINARY(16) NOT NULL,
  PRIMARY KEY (`player`, `gang`),
  INDEX `fk_players_has_gangs_gangs1_idx` (`gang` ASC),
  INDEX `fk_players_has_gangs_players1_idx` (`player` ASC),
  CONSTRAINT `fk_players_has_gangs_players1`
    FOREIGN KEY (`player`)
    REFERENCES `players` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_players_has_gangs_gangs1`
    FOREIGN KEY (`gang`)
    REFERENCES `gangs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gang_relationships`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gang_relationships` (
  `me` BINARY(16) NOT NULL,
  `target` BINARY(16) NOT NULL,
  `relationship` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`me`, `target`),
  INDEX `fk_gangs_has_gangs_gangs2_idx` (`target` ASC),
  INDEX `fk_gangs_has_gangs_gangs1_idx` (`me` ASC),
  CONSTRAINT `fk_gangs_has_gangs_gangs1`
    FOREIGN KEY (`me`)
    REFERENCES `gangs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gangs_has_gangs_gangs2`
    FOREIGN KEY (`target`)
    REFERENCES `gangs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
