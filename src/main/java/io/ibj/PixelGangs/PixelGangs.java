package io.ibj.PixelGangs;

import io.ibj.JLib.JLib;
import io.ibj.JLib.JPlug;
import io.ibj.JLib.db.AuthenticatedDatabaseConnectionDetails;
import io.ibj.JLib.db.DatabaseConnectionDetails;
import io.ibj.JLib.db.DatabaseType;
import io.ibj.JLib.db.utils.ScriptRunner;
import io.ibj.JLib.db.utils.TableUtils;
import io.ibj.JLib.file.ResourceFile;
import io.ibj.JLib.file.ResourceReloadHook;
import io.ibj.JLib.file.YAMLFile;
import io.ibj.JLib.utils.Colors;
import io.ibj.PixelGangs.cmd.CrewChatCmd;
import io.ibj.PixelGangs.cmd.CrewsCmd;
import io.ibj.PixelGangs.cmd.TagCmd;
import io.ibj.PixelGangs.hooks.ActivePlotMeHook;
import io.ibj.PixelGangs.hooks.PlotHook;
import io.ibj.PixelGangs.listeners.ChatListener;
import io.ibj.PixelGangs.listeners.DeathKillListener;
import io.ibj.PixelGangs.listeners.DisconnectListener;
import io.ibj.PixelGangs.persistance.GGangCollection;
import io.ibj.PixelGangs.persistance.GPlayerCollection;
import io.ibj.PixelGangs.persistance.sql.GSqlGangCollection;
import io.ibj.PixelGangs.persistance.sql.GSqlPlayerCollection;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.io.BufferedReader;
import java.sql.Connection;
import java.util.*;

/**
 * Created by joe on 11/30/14.
 */
public class PixelGangs extends JPlug {

    
    private static PixelGangs i;

    
    private GGangCollection gangs;

    
    private GPlayerCollection players;

    private DatabaseConnectionDetails<Connection> dbDetails;

    
    private PlotHook plotHook;

    
    private int gangLimit;

    
    private List<String> ignoredWorlds;

    
    private String neutralTag;

    
    private String enemyTag;

    
    private String allyTag;

    
    private String personalRepPhrase;

    
    private String teamRepPhrase;

    @Getter
    private int gangHelpPageSize;

    @Getter
    private HashMap<UUID,CrewChatCmd.ChatType> chatModes;


    public Connection getDb(){
        return JLib.getI().getDatabaseManager().getConnection(dbDetails);
    }
    
    public static PixelGangs getI(){
        return i;
    }

    public GGangCollection getGangs() {
        return gangs;
    }

    public GPlayerCollection getPlayers() {
        return players;
    }

    public DatabaseConnectionDetails<Connection> getDbDetails() {
        return dbDetails;
    }

    public PlotHook getPlotHook() {
        return plotHook;
    }

    public int getGangLimit() {
        return gangLimit;
    }

    public List<String> getIgnoredWorlds() {
        return ignoredWorlds;
    }

    public String getNeutralTag() {
        return neutralTag;
    }

    public String getEnemyTag() {
        return enemyTag;
    }

    public String getAllyTag() {
        return allyTag;
    }

    public String getPersonalRepPhrase() {
        return personalRepPhrase;
    }

    public String getTeamRepPhrase() {
        return teamRepPhrase;
    }

    @Override
    protected void onModuleEnable() throws Exception {
        i = this;
        chatModes = new HashMap<>();
        getLogger().info("Loading configuration...");
        registerResource(new YAMLFile(this, "config.yml", new ResourceReloadHook() {
            @Override
            public void onReload(ResourceFile resourceFile) {
                YAMLFile file = ((YAMLFile) resourceFile);
                gangLimit = file.getConfig().getInt("gangLimit", 20);
                gangHelpPageSize = file.getConfig().getInt("gangHelpPageSize");
                ignoredWorlds = file.getConfig().getStringList("ignoredWorlds");
                if(ignoredWorlds == null){
                    ignoredWorlds = new ArrayList<>(0);
                }
                if (file.getConfig().getString("sql.username") != null) {
                    dbDetails = new AuthenticatedDatabaseConnectionDetails<>(DatabaseType.SQL, Connection.class,
                            file.getConfig().getString("sql.host", "localhost"),
                            file.getConfig().getInt("sql.port", 3306),
                            file.getConfig().getString("sql.database", ""),
                            file.getConfig().getString("sql.username"),
                            file.getConfig().getString("sql.password"));
                } else {
                    dbDetails = new DatabaseConnectionDetails<>(DatabaseType.SQL, Connection.class,
                            file.getConfig().getString("sql.host", "localhost"),
                            file.getConfig().getInt("sql.port", 3306),
                            file.getConfig().getString("sql.database", ""));
                }
                neutralTag = Colors.colorify(file.getConfig().getString("gang.neutral",""));
                allyTag = Colors.colorify(file.getConfig().getString("gang.ally",""));
                enemyTag = Colors.colorify(file.getConfig().getString("gang.enemy",""));
                personalRepPhrase = Colors.colorify(file.getConfig().getString("rep.personal"));
                teamRepPhrase = Colors.colorify(file.getConfig().getString("rep.team"));
            }
        }));
        reload();
        gangs = new GSqlGangCollection();
        players = new GSqlPlayerCollection();
        getLogger().info("Registering command...");
        registerCmd(CrewsCmd.class);
        registerCmd(TagCmd.class);
        getLogger().info("Initializing DB.");
        initDB();
        getLogger().info("Finished initializing DB.");
        getLogger().info("Registering listeners...");
        registerEvents(new DeathKillListener());
        registerEvents((Listener) players);
        registerEvents(new DisconnectListener());
        registerEvents(new ChatListener());
        getLogger().info("Finished listeners.");
        getLogger().info("Attempting plot hook...");
        if(Bukkit.getPluginManager().isPluginEnabled("Plotz")){
            plotHook = new ActivePlotMeHook();
            getLogger().info("Loading PlotZ hook.");
        }
        else
        {
            plotHook = new PlotHook();
            getLogger().info("Loading empty hook.");
        }
        for(Player p : Bukkit.getOnlinePlayers()){
            ((GSqlPlayerCollection) players).join(p);
        }
    }

    @SneakyThrows
    public void initDB(){
        try(Connection conn = getDb(); BufferedReader dbFileReader = new BufferedReader(getTextResource("db_init.sql"))){
            new ScriptRunner(conn,false,false).runScript(dbFileReader);
            if(!TableUtils.getColumnNames("players",conn).contains("tagsEnabled")){
                TableUtils.insertColumnIntoTable("players","tagsEnabled","TINYINT(1) NULL DEFAULT 1","",conn);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
