package io.ibj.PixelGangs.listeners;

import io.ibj.PixelGangs.PixelGangs;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Aaron.
 */
public class DisconnectListener implements Listener{
    @EventHandler
    public void onKick(PlayerKickEvent e){
        PixelGangs.getI().getChatModes().remove(e.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        PixelGangs.getI().getChatModes().remove(e.getPlayer().getUniqueId());
    }
}
