package io.ibj.PixelGangs.listeners;

import io.ibj.JLib.ThreadLevel;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.TitleManager;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.projectiles.ProjectileSource;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by joe on 12/3/14.
 */
public class DeathKillListener implements Listener {
    @EventHandler
    public void onKill(final PlayerDeathEvent e){
        if(PixelGangs.getI().getIgnoredWorlds().contains(e.getEntity().getWorld().getName())){
            return;
        }
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer killed = PixelGangs.getI().getPlayers().fromPlayer(e.getEntity());
                killed.incDeaths();
                GGang killedGang = killed.getGang();
                if(killedGang != null){
                    killedGang.incDeaths();
                }
                List<MetadataValue> data =  e.getEntity().getMetadata("lastPlayerDamagerCause");
                if(data == null || data.size() == 0){
                    return;
                }
                EntityDamageEvent lastDamageCause = e.getEntity().getLastDamageCause();
                if(lastDamageCause == null){
                    return;
                }
                if(!Objects.equals(data.get(0).asString(), lastDamageCause.getCause().name())){ //Not last damage cause
                    return;
                }
                data =  e.getEntity().getMetadata("lastPlayerDamager");
                if(data == null || data.size() == 0){
                    return;
                }
                UUID id = (UUID) data.get(0).value();
                if(id != null){

                    GPlayer gKiller = PixelGangs.getI().getPlayers().fromID(id);
                    if (gKiller != killed) {
                        gKiller.incKills();
                        double previousRep = gKiller.getReputation();
                        eloCalc(gKiller, killed);
                        double deltaRep = gKiller.getReputation()-previousRep;
                        String personalRep = PixelGangs.getI().getPersonalRepPhrase().replace("<rep>", Double.toString(Math.round(deltaRep*10)/10));
                        String teamRep = null;
                        GGang killerGang = gKiller.getGang();
                        if (killerGang != null) {
                            killerGang.incKills();
                            if (killedGang != null) {
                                double previousRepTeam = killerGang.getReputation();
                                eloCalc(killerGang, killedGang);
                                teamRep = PixelGangs.getI().getTeamRepPhrase().replace("<rep>",Double.toString(Math.round((killerGang.getReputation()-previousRepTeam)*10)/10));
                            }
                        }
                        Player player = Bukkit.getPlayer(id);
                        if(player != null) {
                            if (teamRep == null) {
                                PixelCore.getI().getPlayerManager().getPlayer(player).getTitleManager().sendMovingHologram(new TitleManager.HologramContext(e.getEntity().getLocation().add(0,0.5,0)),personalRep);
                            }
                            else
                            {
                                PixelCore.getI().getPlayerManager().getPlayer(player).getTitleManager().sendMovingHologram(new TitleManager.HologramContext(e.getEntity().getLocation().add(0,0.5,0)),personalRep,teamRep);
                            }
                        }
                    }
                }
            }
        }, ThreadLevel.ASYNC);
    }

    public void eloCalc(GPlayer killer, GPlayer killed){
        double expectedScore = expectedScore(killer.getReputation(),killed.getReputation());
        killer.setReputation(killer.getReputation()+16*(1-expectedScore));
        killed.setReputation(killed.getReputation()+16*(-expectedScore));
    }

    public void eloCalc(GGang killer, GGang killed){
        double expectedScore = expectedScore(killer.getReputation(),killed.getReputation());
        killer.setReputation(killer.getReputation()+16*(1-expectedScore));
        killed.setReputation(killed.getReputation()+16*(-expectedScore));
    }

    private double expectedScore(Double me, Double opponent){
        return 1/(1+Math.pow(10,(opponent-me)/400));
    }

    @EventHandler
    public void onFriendlyFire(EntityDamageByEntityEvent e)
    {
        if (PixelGangs.getI().getIgnoredWorlds().contains(e.getEntity().getWorld().getName())) {
            return;
        }
        if ((e.getEntity() instanceof Player))
        {
            Player damaged = (Player)e.getEntity();
            GPlayer gDamaged = PixelGangs.getI().getPlayers().fromPlayer(damaged);
            if (gDamaged.getGang() == null) {
                return;
            }
            GPlayer attackingPlayer = determineAttackingGang(e);
            if (attackingPlayer != null)
            {
                if ((!attackingPlayer.getFriendlyFire()) &&
                        (gDamaged.getGang().isMember(attackingPlayer.getId()))) {
                    e.setCancelled(true);
                }
                GGang attackingGang = attackingPlayer.getGang();
                if ((attackingGang != null) && (
                        (gDamaged.getGang().isAlly(attackingGang)) || (attackingGang.isAlly(gDamaged.getGang())))) {
                    e.setCancelled(true);
                }
            }
        }
    }
    
    public GPlayer determineAttackingGang(EntityDamageByEntityEvent e){
        if(e.getDamager() instanceof Player){
            return PixelGangs.getI().getPlayers().fromPlayer((Player) e.getDamager()); //fromPlayer should not return null
        }
        else if(e.getDamager() instanceof Projectile){
            ProjectileSource source = ((Projectile) e.getDamager()).getShooter();//Deprecated, but since the return is a ProjectileSource for the nonDeprecated, should cast fine.
            if(source instanceof Player){
                return PixelGangs.getI().getPlayers().fromPlayer((Player) source);
            }
        }
        return null;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void lastDamageEvent(EntityDamageByEntityEvent e){
        if(PixelGangs.getI().getIgnoredWorlds().contains(e.getEntity().getWorld().getName())){
            return;
        }
        if(e.getEntity() instanceof Player){
            if(e.getDamager() instanceof Player){
                e.getEntity().setMetadata("lastPlayerDamager",new FixedMetadataValue(PixelGangs.getI(),e.getDamager().getUniqueId()));
                e.getEntity().setMetadata("lastPlayerDamagerCause",new FixedMetadataValue(PixelGangs.getI(),e.getCause().name()));
            }
            else if(e.getDamager() instanceof Projectile){
                if(((Projectile) e.getDamager()).getShooter() instanceof Player){
                    e.getEntity().setMetadata("lastPlayerDamager",new FixedMetadataValue(PixelGangs.getI(),((Player) ((Projectile) e.getDamager()).getShooter()).getUniqueId()));
                    e.getEntity().setMetadata("lastPlayerDamagerCause",new FixedMetadataValue(PixelGangs.getI(),e.getCause().name()));
                }
            }
        }
    }
}
