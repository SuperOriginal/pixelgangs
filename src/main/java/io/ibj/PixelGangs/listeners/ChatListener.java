package io.ibj.PixelGangs.listeners;

import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.cmd.CrewChatCmd;
import io.ibj.PixelGangs.persistance.GPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Aaron.
 */
public class ChatListener implements Listener{
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent e){
        if(e.isCancelled()) return;
        CrewChatCmd.ChatType type = CrewChatCmd.getChatType(e.getPlayer().getUniqueId());
        if(type == CrewChatCmd.ChatType.PUBLIC) return;

        e.setCancelled(true);
        GPlayer sender = PixelGangs.getI().getPlayers().fromPlayer(e.getPlayer());

        for(Player p : Bukkit.getServer().getOnlinePlayers()){
            GPlayer receiver = PixelGangs.getI().getPlayers().fromPlayer(p);
            boolean allowed = true;
            if(type == CrewChatCmd.ChatType.CREW && (receiver.getGang() == null || !receiver.getGang().isMember(e.getPlayer().getUniqueId()))) allowed = false;

            if(type == CrewChatCmd.ChatType.ALLY){
                if(receiver.getGang() == null) allowed = false;
                else if(sender.getGang() == null) allowed = false;
                else if(!receiver.getGang().isMember(e.getPlayer().getUniqueId()) && !receiver.getGang().isAlly(sender.getGang())) allowed = false;
            }

            if(p.getName().equals(e.getPlayer().getName())) allowed = false;

            if(allowed){
                if(type == CrewChatCmd.ChatType.ALLY){
                    PixelGangs.getI().getF("gang.chat.allyChatFormat").replace("<crew>",PixelGangs.getI().getPlayers().fromPlayer(e.getPlayer()).getGang().getName()).replace("<name>", e.getPlayer().getName()).replace("<message>",e.getMessage()).sendTo(p);
                }
                if(type == CrewChatCmd.ChatType.CREW){
                    PixelGangs.getI().getF("gang.chat.crewChatFormat").replace("<crew>", PixelGangs.getI().getPlayers().fromPlayer(e.getPlayer()).getGang().getName()).replace("<name>",e.getPlayer().getDisplayName()).replace("<message>",e.getMessage()).sendTo(p);
                }
            }
        }

        if(type == CrewChatCmd.ChatType.ALLY){
            PixelGangs.getI().getF("gang.chat.allyChatFormat").replace("<crew>", PixelGangs.getI().getPlayers().fromPlayer(e.getPlayer()).getGang().getName()).replace("<name>", e.getPlayer().getName()).replace("<message>",e.getMessage()).sendTo(e.getPlayer());
        }
        if(type == CrewChatCmd.ChatType.CREW){
            PixelGangs.getI().getF("gang.chat.crewChatFormat").replace("<crew>", PixelGangs.getI().getPlayers().fromPlayer(e.getPlayer()).getGang().getName()).replace("<name>",e.getPlayer().getDisplayName()).replace("<message>",e.getMessage()).sendTo(e.getPlayer());
        }
    }
}
