package io.ibj.PixelGangs.persistance;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by joe on 11/30/14.
 */
public interface GPlayerCollection {

    GPlayer fromPlayer(Player p);
    GPlayer fromPlayer(OfflinePlayer p);
    GPlayer fromID(UUID id);
    GPlayer fromName(String name);

    void cache(GPlayer player);
    void decache(GPlayer player);
}