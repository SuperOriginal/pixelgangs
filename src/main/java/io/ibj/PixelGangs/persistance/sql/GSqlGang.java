package io.ibj.PixelGangs.persistance.sql;

import com.google.common.collect.ImmutableSet;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.format.Format;
import io.ibj.JLib.utils.UUIDUtils;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.utils.StringTests;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import io.ibj.PixelGangs.persistance.GangRelationship;
import io.ibj.PixelGangs.persistance.GangRole;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Created by joe on 11/30/14.
 */
@EqualsAndHashCode(of={"id"})
public class GSqlGang implements GGang {

    @Getter
    UUID id;
    @Getter
    String name;
    @Getter
    String description;
    @Getter
    Integer kills;
    @Getter
    Integer deaths;
    @Getter
    Double reputation;


    Location safehouse;

    Map<UUID, GangRelationship> relationshipMap;

    Set<UUID> memberCache;


    @Override
    public String getTagName() {
        return getName().length() > 8 ? getName().substring(0,4) : getName();
    }

    @Override
    @SneakyThrows
    public void setName(String name, GPlayer executor) {
        StringTests.testContents(name);
        if(name.length() > 16){
            throw new PlayerException(PixelGangs.getI().getF("gang.setName.tooLong").replace("<name>",name));
        }
        GGang test = PixelGangs.getI().getGangs().getByName(name);
        if(test != null){
            throw new PlayerException(PixelGangs.getI().getF("gang.setName.alreadyExists").replace("<name>",name));
        }
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("update gangs set name = ? where id = ?;");
            statement.setString(1,name);
            statement.setBytes(2, UUIDUtils.toBytes(id));
            statement.execute();
        }
        this.name = name;
        sendMessageToGang(PixelGangs.getI().getF("gang.setName.update").replace("<name>",name).replace("<exe>", executor.getDisplayName()));
    }

    @Override
    @SneakyThrows
    public void setDescription(String desc, GPlayer executor) {
        StringTests.testContents(desc);
        if(desc.length() > 255){
            throw new PlayerException(PixelGangs.getI().getF("gang.setDescription.tooLong"));
        }
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("update gangs set `desc` = ? where id = ?;");
            statement.setString(1,desc);
            statement.setBytes(2,UUIDUtils.toBytes(id));
            statement.execute();
        }
        this.description = desc;
        sendMessageToGang(PixelGangs.getI().getF("gang.setDescription.update").replace("<desc>",desc)
                .replace("<exe>", executor.getDisplayName()));
    }

    @Override
    @SneakyThrows
    public void incKills() {
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("update gangs set kills = kills + 1 where id = ?");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            statement.execute();
        }
        kills++;
    }

    @Override
    @SneakyThrows
    public void incDeaths() {
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("update gangs set deaths = deaths + 1 where id = ?");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            statement.execute();
        }
        deaths++;
    }

    @Override
    @SneakyThrows
    public void setReputation(Double reputation) {
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("update gangs set reputation = ? where id = ?;");
            statement.setDouble(1,reputation);
            statement.setBytes(2,UUIDUtils.toBytes(id));
            statement.execute();
        }
        this.reputation = reputation;
    }

    @Override
    @SneakyThrows
    public Set<GPlayer> getMembers() {
        ImmutableSet.Builder<GPlayer> builder = ImmutableSet.builder();
        for(UUID id : memberCache){
            builder.add(PixelGangs.getI().getPlayers().fromID(id));
        }
        return builder.build();
    }

    @Override
    public boolean isMember(UUID member) {
        return memberCache.contains(member);
    }


    @Override
    public Set<GGang> getEnemies() {
        ImmutableSet.Builder<GGang> builder = ImmutableSet.builder();
        for(Map.Entry<UUID,GangRelationship> relationship : relationshipMap.entrySet()){
            if(relationship.getValue() == GangRelationship.ENEMY || relationship.getValue() == GangRelationship.ENEMY_SUPER){
                builder.add(PixelGangs.getI().getGangs().getById(relationship.getKey()));
            }
        }
        return builder.build();
    }

    @Override
    public Set<GGang> getAllies() {
        ImmutableSet.Builder<GGang> builder = ImmutableSet.builder();
        for(Map.Entry<UUID,GangRelationship> relationship : relationshipMap.entrySet()){
            if(relationship.getValue() == GangRelationship.ALLY){
                builder.add(PixelGangs.getI().getGangs().getById(relationship.getKey()));
            }
        }
        return builder.build();
    }

    @Override
    public Set<GGang> getAllySentRequests() {
        ImmutableSet.Builder<GGang> builder = ImmutableSet.builder();
        for(Map.Entry<UUID,GangRelationship> relationship : relationshipMap.entrySet()){
            if(relationship.getValue() == GangRelationship.ALLY_REQUEST_SENT){
                builder.add(PixelGangs.getI().getGangs().getById(relationship.getKey()));
            }
        }
        return builder.build();
    }

    @Override
    public Set<GGang> getAllyRecievedRequests() {
        ImmutableSet.Builder<GGang> builder = ImmutableSet.builder();
        for(Map.Entry<UUID,GangRelationship> relationship : relationshipMap.entrySet()){
            if(relationship.getValue() == GangRelationship.ALLY_REQUEST_RECIEVED){
                builder.add(PixelGangs.getI().getGangs().getById(relationship.getKey()));
            }
        }
        return builder.build();
    }

    @Override
    @SneakyThrows
    public void enemy(GGang gang, GPlayer executor) {
        relationshipMap.put(gang.getId(),GangRelationship.ENEMY_SUPER);
        ((GSqlGang) gang).relationshipMap.put(id,GangRelationship.ENEMY);

        updateAllTags(gang);

        //Ok. So we need to first remove the gang pair as allies, then remove any requests, then add as enemies. Its complex, ok?!
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("insert into gang_relationships (me, target, relationship) values (?,?,?) on duplicate key update relationship = ?;");
            byte[] id_bytes = UUIDUtils.toBytes(id);
            statement.setBytes(1, id_bytes);
            byte[] gang_bytes = UUIDUtils.toBytes(gang.getId());
            statement.setBytes(2, gang_bytes);
            statement.setInt(3, GangRelationship.ENEMY_SUPER.id);
            statement.setInt(4, GangRelationship.ENEMY_SUPER.id);
            statement.execute();
            statement.setBytes(1, gang_bytes);
            statement.setBytes(2, id_bytes);
            statement.setInt(3,GangRelationship.ENEMY.id);
            statement.setInt(4, GangRelationship.ENEMY.id);
            statement.execute();
            sendMessageToGang(PixelGangs.getI().getF("gang.enemy.myGang").replace("<exe>",executor.getDisplayName()).replace("<gang>",gang.getName()));
            gang.sendMessageToGang(PixelGangs.getI().getF("gang.enemy.otherGang").replace("<exe>",executor.getDisplayName()).replace("<gang>",getName()));
        }
    }

    private void updateAllTags(GGang gang) {
        for(GPlayer player : gang.getMembers()){
            player.updateTheirTags();
        }
        for(GPlayer player : getMembers()){
            player.updateTheirTags();
        }
    }

    @Override
    @SneakyThrows
    public void neutral(GGang gang, GPlayer executor) {
        GangRelationship gangRelationship = relationshipMap.get(gang.getId());
        if(gangRelationship == GangRelationship.ENEMY){
            throw new PlayerException(PixelGangs.getI().getF("gang.neutral.enemy").replace("<gang>", gang.getName()));
        }
        else if(gangRelationship == null){
            throw new PlayerException(PixelGangs.getI().getF("gang.neutral.alreadyNeutral").replace("<gang>", gang.getName()));
        }
        relationshipMap.remove(gang.getId());
        ((GSqlGang) gang).relationshipMap.remove(id);
        updateAllTags(gang);
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("delete from gang_relationships where (me = ? and target = ?) or (me = ? and target = ?);");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            statement.setBytes(2,UUIDUtils.toBytes(gang.getId()));
            statement.setBytes(3,UUIDUtils.toBytes(gang.getId()));
            statement.setBytes(4, UUIDUtils.toBytes(id));
            statement.execute();
            sendMessageToGang(PixelGangs.getI().getF("gang.neutral.neutraledLocal").replace("<exe>",executor.getDisplayName()).replace("<gang>", gang.getName()));
            gang.sendMessageToGang(PixelGangs.getI().getF("gang.neutral.neutraledRemote").replace("<exe>",executor.getDisplayName())
                    .replace("<gang>", this.getName()));
        }
    }

    @Override
    public boolean isEnemy(GGang gang) {
        GangRelationship relationship = relationshipMap.get(gang.getId());
        return relationship == GangRelationship.ENEMY || relationship == GangRelationship.ENEMY_SUPER;
    }

    @Override
    public boolean isAlly(GGang gang) {
        return relationshipMap.get(gang.getId()) == GangRelationship.ALLY;
    }

    @Override
    @SneakyThrows
    public void allyInvite(GGang gang, GPlayer executor) {
        GangRelationship relationship = relationshipMap.get(gang.getId());
        if(relationship == GangRelationship.ENEMY_SUPER){
            throw new PlayerException(PixelGangs.getI().getF("gang.allyInvite.enemied").replace("<gang>", gang.getName()));
        }
        else if(relationship == GangRelationship.ENEMY){
            PixelGangs.getI().getF("gang.allyInvite.enemyRelationshipRemoved").sendTo(executor.getId());
        }
        else if(relationship == GangRelationship.ALLY){
            throw new PlayerException(PixelGangs.getI().getF("gang.allyInvite.alreadyAllied").replace("<gang>",gang.getName()));
        }
        else if(relationship == GangRelationship.ALLY_REQUEST_RECIEVED){
            acceptAlly(gang, executor);
            return;
        }
        relationshipMap.put(gang.getId(),GangRelationship.ALLY_REQUEST_SENT);
        ((GSqlGang) gang).relationshipMap.put(id,GangRelationship.ALLY_REQUEST_RECIEVED);
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("insert into gang_relationships (me, target, relationship) values (?,?,?) on duplicate key update relationship = ?;");
            byte[] id_bytes = UUIDUtils.toBytes(id);
            statement.setBytes(1, id_bytes);
            byte[] gang_bytes = UUIDUtils.toBytes(gang.getId());
            statement.setBytes(2, gang_bytes);
            statement.setInt(3, GangRelationship.ALLY_REQUEST_SENT.id);
            statement.setInt(4, GangRelationship.ALLY_REQUEST_SENT.id);
            statement.execute();
            statement.setBytes(1, gang_bytes);
            statement.setBytes(2, id_bytes);
            statement.setInt(3,GangRelationship.ALLY_REQUEST_RECIEVED.id);
            statement.setInt(4, GangRelationship.ALLY_REQUEST_RECIEVED.id);
            statement.execute();
            sendMessageToGang(PixelGangs.getI().getF("gang.allyInvite.allyInviteLocal").replace("<exe>",executor.getDisplayName()).replace("<gang>",gang.getName()));
            gang.sendMessageToGang(PixelGangs.getI().getF("gang.allyInvite.allyInviteRemote").replace("<exe>",executor.getDisplayName()).replace("<gang>",getName()));
        }
    }

    @Override
    @SneakyThrows
    public void acceptAlly(GGang gang, GPlayer executor){
        GangRelationship relationship = relationshipMap.get(gang.getId());
        if(relationship == GangRelationship.ALLY){
            throw new PlayerException(PixelGangs.getI().getF("gang.allyAccept.alreadyAllied").replace("<gang>",gang.getName()));
        }
        else if(relationship == GangRelationship.ALLY_REQUEST_SENT){
            throw new PlayerException(PixelGangs.getI().getF("gang.allyAccept.alreadySentRequest").replace("<gang>",gang.getName()));
        }
        else if(relationship == GangRelationship.ENEMY || relationship == GangRelationship.ENEMY_SUPER){
            throw new PlayerException(PixelGangs.getI().getF("gang.allyAccept.enemies").replace("<gang>",gang.getName()));
        }
        else if(relationship == GangRelationship.ALLY_REQUEST_RECIEVED){
            relationshipMap.put(gang.getId(),GangRelationship.ALLY);
            ((GSqlGang) gang).relationshipMap.put(getId(),GangRelationship.ALLY);
            updateAllTags(gang);
            try(Connection conn = PixelGangs.getI().getDb()){
                PreparedStatement statement = conn.prepareStatement("update gang_relationships set relationship = ? where (me = ? and target = ?) or (me = ? and target = ?);");
                statement.setInt(1,GangRelationship.ALLY.id);
                statement.setBytes(2, UUIDUtils.toBytes(id));
                statement.setBytes(3,UUIDUtils.toBytes(gang.getId()));
                statement.setBytes(4,UUIDUtils.toBytes(gang.getId()));
                statement.setBytes(5,UUIDUtils.toBytes(id));
                statement.execute();
                gang.sendMessageToGang(PixelGangs.getI().getF("gang.ally.otherTeam").replace("<player>",executor.getDisplayName()).replace("<gang>",getName()));
                sendMessageToGang(PixelGangs.getI().getF("gang.ally.myTeam").replace("<player>",executor.getDisplayName()).replace("<gang>", gang.getName()));
            }
        }
        else
        {
            allyInvite(gang,executor);
        }
    }

    @Override
    public Location getSafehouse() {
        PixelGangs.getI().getPlotHook().testOwner(safehouse,memberCache);
        return safehouse;
    }

    @Override
    @SneakyThrows
    public void setSafehouse(Location l, GPlayer executor) {
        if(l != null) {
            safehouse = l.clone();
            PixelGangs.getI().getPlotHook().testOwner(safehouse,memberCache);
            try (Connection conn = PixelGangs.getI().getDb()) {
                PreparedStatement statement = conn.prepareStatement("INSERT INTO safehouses (id, x, y, z, pitch, yaw, world) VALUES (?,?,?,?,?,?,?) " +
                        "ON DUPLICATE KEY UPDATE x = ?, y = ?, z = ?, pitch = ?, yaw = ?, world = ?;");
                statement.setBytes(1, UUIDUtils.toBytes(id));
                statement.setDouble(2, l.getX());
                statement.setDouble(3, l.getY());
                statement.setDouble(4, l.getZ());
                statement.setFloat(5, l.getPitch());
                statement.setFloat(6, l.getYaw());
                statement.setBytes(7, UUIDUtils.toBytes(l.getWorld().getUID()));
                statement.setDouble(8, l.getX());
                statement.setDouble(9, l.getY());
                statement.setDouble(10, l.getZ());
                statement.setFloat(11, l.getPitch());
                statement.setFloat(12, l.getYaw());
                statement.setBytes(13, UUIDUtils.toBytes(l.getWorld().getUID()));
                statement.execute();
                sendMessageToGang(PixelGangs.getI().getF("gang.safehouseSet.set").replace("<exe>",executor.getDisplayName()));
            }
        }
        else
        {
            safehouse = null;
            try(Connection conn = PixelGangs.getI().getDb()){
                PreparedStatement statement = conn.prepareStatement("delete from safehouses where id = ?;");
                statement.setBytes(1,UUIDUtils.toBytes(id));
                statement.execute();
                sendMessageToGang(PixelGangs.getI().getF("gang.safehouseSet.unset").replace("<exe>",executor.getDisplayName()));
            }
        }
    }

    @Override
    public void disband(GPlayer executor) {
        sendMessageToGang(PixelGangs.getI().getF("gang.disband.success").replace("<exe>",executor.getDisplayName()));
        disband();
    }

    @Override
    @SneakyThrows
    public void disband() {
        for(UUID r_id : relationshipMap.keySet()){
            GGang gang = PixelGangs.getI().getGangs().getById(r_id);
            ((GSqlGang) gang).relationshipMap.remove(id);
        }
        for(GPlayer player : getMembers()){
            GSqlPlayer pCast = ((GSqlPlayer) player);
            pCast.gangId = null;
            player.updateMyTag();
        }

        try(Connection conn = PixelGangs.getI().getDb()){
            byte[] id_bytes = UUIDUtils.toBytes(id);
            PreparedStatement statement = conn.prepareStatement("delete from gang_relationships where me = ? or target = ?");
            statement.setBytes(1,id_bytes);
            statement.setBytes(2,id_bytes);
            statement.execute();
            statement = conn.prepareStatement("delete from gang_invitations where gang = ?;");
            statement.setBytes(1,id_bytes);
            statement.execute();
            statement = conn.prepareStatement("update players set gang = ? where gang = ?;");
            statement.setBytes(1,null);
            statement.setBytes(2,id_bytes);
            statement.execute();
            statement = conn.prepareStatement("delete from safehouses where id = ?;");
            statement.setBytes(1,id_bytes);
            statement.execute();
            statement = conn.prepareStatement("delete FROM gangs where id = ?;");
            statement.setBytes(1, id_bytes);
            statement.execute();
        }
    }

    @Override
    @SneakyThrows
    public Set<GPlayer> getInvitations() {
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select player from gang_invitations where gang = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            try(ResultSet set = statement.executeQuery()){
                ImmutableSet.Builder<GPlayer> builder = ImmutableSet.builder();
                while(set.next()){
                    builder.add(PixelGangs.getI().getPlayers().fromID(UUIDUtils.fromBytes(set.getBytes("player"))));
                }
                return builder.build();
            }
        }
    }

    @Override
    @SneakyThrows
    public void invite(GPlayer player, GPlayer executor) {
        GGang gang = player.getGang();
        if(gang != null){
            if(gang == this){
                throw new PlayerException(PixelGangs.getI().getF("gang.invite.alreadyInThisGang").replace("<target>",player.getDisplayName()));
            }
            throw new PlayerException(PixelGangs.getI().getF("gang.invite.alreadyInAnotherGang").replace("<target>",player.getDisplayName())
                    .replace("<target_gang>",gang.getName()));
        }
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select count(*) from gang_invitations where gang = ? and player = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            statement.setBytes(2,UUIDUtils.toBytes(player.getId()));
            try(ResultSet set = statement.executeQuery()){
                set.next();
                if(set.getInt(1) > 0){
                    throw new PlayerException(PixelGangs.getI().getF("gang.invite.alreadyInvited").replace("<target>",player.getDisplayName()));
                }
            }
            statement = conn.prepareStatement("insert into gang_invitations (player, gang) values (?,?);");
            statement.setBytes(1,UUIDUtils.toBytes(player.getId()));
            statement.setBytes(2,UUIDUtils.toBytes(id));
            statement.execute();
            sendMessageToGang(PixelGangs.getI().getF("gang.invite.teamMessage").replace("<exe>", executor.getDisplayName())
                    .replace("<target>", player.getDisplayName()));
            PixelGangs.getI().getF("gang.invite.individualMessage").replace("<exe>",executor.getDisplayName())
                        .replace("<gang>",this.getName()).sendTo(player.getId());
        }
    }

    @Override
    @SneakyThrows
    public void deinvite(GPlayer player, GPlayer executor) {
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select count(*) from gang_invitations where gang = ? and player = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            statement.setBytes(2,UUIDUtils.toBytes(player.getId()));
            try(ResultSet set = statement.executeQuery()){
                set.next();
                if(set.getInt(1) == 0){
                    throw new PlayerException(PixelGangs.getI().getF("gang.deinvite.notInvited").replace("<target>",player.getDisplayName()));
                }
            }
            statement = conn.prepareStatement("delete from gang_invitations where player = ? and gang = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(player.getId()));
            statement.setBytes(2,UUIDUtils.toBytes(id));
            sendMessageToGang(PixelGangs.getI().getF("gang.deinvite.message").replace("<target>",player.getDisplayName())
                    .replace("<exe>",executor.getDisplayName()));
        }
    }

    @Override
    @SneakyThrows
    public void join(GPlayer player) {
        GGang gang = player.getGang();
        if(gang != null){
            if(gang == this){
                throw new PlayerException(PixelGangs.getI().getF("gang.join.alreadyJoined"));
            }
            else
            {
                throw new PlayerException(PixelGangs.getI().getF("gang.join.alreadyJoinedDifferent").replace("<gang>",gang.getName()));
            }
        }
        if(memberCache.size() > PixelGangs.getI().getGangLimit()){
            sendMessageToGang(PixelGangs.getI().getF("gang.join.tooManyPlayersTeam").replace("<player>",player.getDisplayName()));
            throw new PlayerException(PixelGangs.getI().getF("gang.join.tooManyPlayers").replace("<gang>",getName()));
        }
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select count(*) from gang_invitations where gang = ? and player = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            statement.setBytes(2,UUIDUtils.toBytes(player.getId()));
            try(ResultSet set = statement.executeQuery()){
                set.next();
                if(set.getInt(1) < 1){
                    throw new PlayerException(PixelGangs.getI().getF("gang.join.notInvited").replace("<gang>",getName()));
                }
            }
            statement = conn.prepareStatement("update players set gang = ?, role = ? where id = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            statement.setInt(2, GangRole.MEMBER.getRelation());
            statement.setBytes(3, UUIDUtils.toBytes(player.getId()));
            statement.execute();
            statement = conn.prepareStatement("delete from gang_invitations where player = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(player.getId()));
            statement.execute();
            ((GSqlPlayer) player).gangId = id;
            ((GSqlPlayer) player).role = GangRole.MEMBER;
            player.updateMyTag();
            memberCache.add(player.getId());
            sendMessageToGang(PixelGangs.getI().getF("gang.join.joinedGang").replace("<target>",player.getName()));
        }
    }

    @Override
    @SneakyThrows
    public void kick(GPlayer player, GPlayer executor) {
        GGang gang = player.getGang();
        if(gang != this){
            if(gang == null){
                throw new PlayerException(PixelGangs.getI().getF("gang.kick.notPartOfGang").replace("<target>",player.getDisplayName()));
            }
            else
            {
                throw new PlayerException(PixelGangs.getI().getF("gang.kick.partOfAnotherGang").replace("<target>",player.getDisplayName()));
            }
        }
        try(Connection conn = PixelGangs.getI().getDb()){
            ((GSqlPlayer) player).gangId = null;
            player.updateMyTag();
            memberCache.remove(player.getId());
            PreparedStatement statement = conn.prepareStatement("update players set gang = ? where id = ?;");
            statement.setBytes(1,null);
            statement.setBytes(2,UUIDUtils.toBytes(player.getId()));
            statement.execute();
            sendMessageToGang(PixelGangs.getI().getF("gang.kick.kickteam").replace("<exe>",executor.getDisplayName())
                    .replace("<target>",player.getDisplayName()));
            PixelGangs.getI().getF("gang.kick.kickPlayer").replace("<exe>",executor.getDisplayName()).replace("<gang>",getName()).sendTo(player.getId());
        }
    }

    @Override
    @SneakyThrows
    public void leave(GPlayer player) {
        GGang gang = player.getGang();
        if(gang != this){
            if(gang == null){
                throw new PlayerException(PixelGangs.getI().getF("gang.leave.notPartOfGang"));
            }
            else
            {
                throw new PlayerException(PixelGangs.getI().getF("gang.leave.partOfAnotherGang"));
            }
        }
        if(player.getRole() == GangRole.LEADER){
            GPlayer bestPlayer = null;
            for(GPlayer p : getMembers()){
                if(p == player){
                    continue;
                }
                if(bestPlayer == null){
                    bestPlayer = p;
                }
                else
                {
                    if(bestPlayer.getRole().getRelation() > p.getRole().getRelation()){
                        continue;
                    }
                    if(bestPlayer.getRole().getRelation() < p.getRole().getRelation()){
                        bestPlayer = p;
                        continue;
                    }
                    if(bestPlayer.getReputation() < p.getReputation()){
                        bestPlayer = p;
                    }
                }
            }
            if(bestPlayer == null){
                disband();
                sendMessageToGang(PixelGangs.getI().getF("gang.disband.success").replace("<exe>",player.getDisplayName()));
                return;
            }
            bestPlayer.setRole(GangRole.LEADER);
        }
        try(Connection conn = PixelGangs.getI().getDb()){

            PreparedStatement statement = conn.prepareStatement("update players set gang = ? where id = ?;");
            statement.setBytes(1,null);
            statement.setBytes(2,UUIDUtils.toBytes(player.getId()));
            statement.execute();
            ((GSqlPlayer) player).gangId = null;
            player.updateMyTag();
            memberCache.remove(player.getId());
            sendMessageToGang(PixelGangs.getI().getF("gang.leave.leaveTeam").replace("<exe>",player.getDisplayName()));
            PixelGangs.getI().getF("gang.leave.leavePlayer").replace("<gang>",getName()).sendTo(player.getId());
        }
    }

    @Override
    public void sendMessageToGang(String message) {
        for(UUID id : memberCache){
            Player p = Bukkit.getPlayer(id);
            if(p != null){
                p.sendMessage(message);
            }
        }
    }

    @Override
    public void sendMessageToGang(Format format) {
        format.sendTo(memberCache.toArray(new UUID[memberCache.size()]));
    }

    @Override
    @SneakyThrows
    /* http://dba.stackexchange.com/questions/13703/get-the-rank-of-a-user-in-a-score-table*/
    public int getCurrentRank() {
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select FIND_IN_SET( reputation, (" +
                    "SELECT GROUP_CONCAT( reputation" +
                    " ORDER by reputation DESC )" +
                    "FROM gangs)" +
                    ") AS rank" +
                    " FROM gangs where id = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            try(ResultSet set = statement.executeQuery()){
                return set.getInt(1);
            }
        }
    }
}
