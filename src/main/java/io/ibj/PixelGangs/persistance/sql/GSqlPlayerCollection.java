package io.ibj.PixelGangs.persistance.sql;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.utils.UUIDUtils;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import io.ibj.PixelGangs.persistance.GPlayerCollection;
import io.ibj.PixelGangs.persistance.GangRole;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by joe on 12/3/14.
 */
public class GSqlPlayerCollection implements GPlayerCollection, Listener {

    private Map<UUID, GSqlPlayer> uuidMap = new HashMap<>();
    private Map<String, GSqlPlayer> nameMap = new HashMap<>();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(final PlayerJoinEvent e){
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            @SneakyThrows
            public void run() {
                join(e.getPlayer());
            }
        }, ThreadLevel.ASYNC);
    }

    public void join(Player e) throws SQLException {
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("insert into players (id, name, displayname, reputation) values (?,?,?,?) on duplicate key update name = ?;");
            statement.setBytes(1, UUIDUtils.toBytes(e.getUniqueId()));
            statement.setString(2,e.getName());
            statement.setString(3,e.getDisplayName());
            statement.setDouble(4,1500D);
            statement.setString(5,e.getName());
            statement.execute();
        }
        GPlayer player = fromPlayer(e);
        cache(player); //Reload full row, and cache.
        GGang gang = player.getGang();
        if(gang != null){
            PixelGangs.getI().getGangs().cache(gang); //Will only cache once, due to cyclical lookup
        }
        player.updateTheirTags();
        player.updateMyTag();
    }


    @EventHandler
    public void onLeave(final PlayerQuitEvent e){
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            @SneakyThrows
            public void run() {
                GPlayer player = fromPlayer(e.getPlayer());
                decache(player);
                if(player.getGang() != null) {
                    boolean needsCleaned = true;
                    for (UUID member : ((GSqlGang) player.getGang()).memberCache) {
                        if(Bukkit.getPlayer(member) != null && member != e.getPlayer().getUniqueId()){
                            needsCleaned = false;
                            break;
                        }
                    }
                    if(needsCleaned){
                        PixelGangs.getI().getGangs().decache(player.getGang());
                    }
                }
                try(Connection conn = PixelGangs.getI().getDb()){
                    PreparedStatement statement = conn.prepareStatement("update players set displayname = ? where id = ?;");
                    statement.setString(1,e.getPlayer().getDisplayName());
                    statement.setBytes(2,UUIDUtils.toBytes(e.getPlayer().getUniqueId()));
                    statement.execute();
                }
            }
        },ThreadLevel.ASYNC);
    }

    @EventHandler
    public void onKick(final PlayerKickEvent e){
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            @SneakyThrows
            public void run() {
                GPlayer player = fromPlayer(e.getPlayer());
                decache(player);
                if(player.getGang() != null) {
                    boolean needsCleaned = true;
                    for (UUID member : ((GSqlGang) player.getGang()).memberCache) {
                        if(Bukkit.getPlayer(member) != null && member != e.getPlayer().getUniqueId()){
                            needsCleaned = false;
                            break;
                        }
                    }
                    if(needsCleaned){
                        PixelGangs.getI().getGangs().decache(player.getGang());
                    }
                }
                try(Connection conn = PixelGangs.getI().getDb()){
                    PreparedStatement statement = conn.prepareStatement("update players set displayname = ? where id = ?;");
                    statement.setString(1,e.getPlayer().getDisplayName());
                    statement.setBytes(2,UUIDUtils.toBytes(e.getPlayer().getUniqueId()));
                    statement.execute();
                }
            }
        },ThreadLevel.ASYNC);
    }


    @Override
    public GPlayer fromPlayer(Player p) {
        return fromID(p.getUniqueId());
    }

    @Override
    public GPlayer fromPlayer(OfflinePlayer p) {
        return fromID(p.getUniqueId());
    }

    @Override
    @SneakyThrows
    public GPlayer fromID(UUID id) {
        GPlayer ret = uuidMap.get(id);
        if(ret != null){
            return ret;
        }
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select * from players where id = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            try(ResultSet set = statement.executeQuery()){
                if(set.next()){
                    return decode(set);
                }
                return null;
            }
        }
    }

    @Override
    @SneakyThrows
    public GPlayer fromName(String name) {
        GPlayer ret = nameMap.get(name);
        if(ret != null){
            return ret;
        }
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select * from players where name = ?;");
            statement.setString(1,name);
            try(ResultSet set = statement.executeQuery()){
                if(set.next()){
                    return decode(set);
                }
                return null;
            }
        }
    }

    @Override
    public void cache(GPlayer player) {
        uuidMap.put(player.getId(), ((GSqlPlayer) player));
        nameMap.put(player.getName(), ((GSqlPlayer) player));
    }

    @Override
    public void decache(GPlayer player) {
        uuidMap.remove(player.getId());
        nameMap.remove(player.getName());
    }

    @SneakyThrows
    private GPlayer decode(ResultSet set){
        UUID id = UUIDUtils.fromBytes(set.getBytes("id"));
        GSqlPlayer ret = uuidMap.get(id);
        if(ret != null){
            return ret;
        }
        ret = new GSqlPlayer();
        ret.id = id;
        ret.name = set.getString("name");
        ret.displayName = set.getString("displayName");
        ret.kills = set.getInt("kills");
        ret.deaths = set.getInt("deaths");
        ret.reputation = set.getDouble("reputation");
        ret.gangId = UUIDUtils.fromBytes(set.getBytes("gang"));
        ret.role = GangRole.fromId(set.getInt("role"));
        ret.friendlyFire = set.getBoolean("friendlyFire");
        return ret;
    }
}
