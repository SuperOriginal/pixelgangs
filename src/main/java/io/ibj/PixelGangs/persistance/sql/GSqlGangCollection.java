package io.ibj.PixelGangs.persistance.sql;

import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.utils.UUIDUtils;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.utils.StringTests;
import io.ibj.PixelGangs.persistance.*;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

/**
 * Created by joe on 12/3/14.
 */
public class GSqlGangCollection implements GGangCollection {

    private Map<UUID, GSqlGang> uuidMap = new HashMap<>();
    private Map<String, GSqlGang> nameMap = new HashMap<>();

    @Override
    @SneakyThrows
    public GGang create(String name, GPlayer leader) {
        StringTests.testContents(name);
        if(name.length() > 16){
            throw new PlayerException(PixelGangs.getI().getF("gang.create.tooLong").replace("<name>",name));
        }
        if(leader.getGang() != null){
            throw new PlayerException(PixelGangs.getI().getF("gang.create.alreadyPartOfGang").replace("<gang>",leader.getGang().getName()));
        }
        if(getByName(name) != null){
            throw new PlayerException(PixelGangs.getI().getF("gang.create.nameTaken"));
        }
        GSqlGang created = new GSqlGang();
        created.id = UUID.randomUUID();
        created.name = name;
        created.description = "Default description D:";
        created.kills = 0;
        created.deaths = 0;
        created.reputation = 1500D;
        created.safehouse = null;
        created.memberCache = new HashSet<>();
        created.memberCache.add(leader.getId());
        created.relationshipMap = new HashMap<>();
        ((GSqlPlayer) leader).gangId = created.id;
        leader.setRole(GangRole.LEADER);
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("insert into gangs (id, name, `desc`, kills, deaths, reputation, safehouse) VALUES (?,?,?,?,?,?,?);");
            statement.setBytes(1, UUIDUtils.toBytes(created.id));
            statement.setString(2, name);
            statement.setString(3, created.description);
            statement.setInt(4,0);
            statement.setInt(5,0);
            statement.setDouble(6,created.reputation);
            statement.setBytes(7,null);
            statement.execute();
            statement = conn.prepareStatement("update players set gang = ? where id = ?");
            statement.setBytes(1,UUIDUtils.toBytes(created.id));
            statement.setBytes(2,UUIDUtils.toBytes(leader.getId()));
            statement.execute();
        }
        cache(created);
        PixelGangs.getI().getF("gang.create.success").replace("<gang>",created.name).sendTo(leader.getId());
        return created;
    }

    @Override
    @SneakyThrows
    public GGang getByName(String name) {
        GGang ret = nameMap.get(name);
        if(ret != null){
            return ret;
        }
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select * from gangs where name = ?;");
            statement.setString(1,name);
            try(ResultSet set = statement.executeQuery()){
                if(set.next()){
                    return decode(set);
                }
                else
                {
                    GPlayer player = PixelGangs.getI().getPlayers().fromName(name);
                    if(player != null){
                        return player.getGang();
                    }
                    return null;
                }
            }
        }
    }

    @Override
    @SneakyThrows
    public GGang getById(UUID id) {
        GGang ret = uuidMap.get(id);
        if(ret != null){
            return ret;
        }
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select * from gangs where id = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            try(ResultSet set = statement.executeQuery()){
                if(set.next()){
                    return decode(set);
                }
                else
                {
                    return null;
                }
            }
        }
    }

    @Override
    @SneakyThrows
    public List<GGang> getLeaderboard() {
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select * from gangs order by reputation DESC LIMIT 10;");
            try(ResultSet set = statement.executeQuery()){
                List<GGang> gangList = new ArrayList<>(10);
                while(set.next()){
                    gangList.add(decode(set));
                }
                return gangList;
            }
        }
    }

    @Override
    public void cache(GGang gang) {
        uuidMap.put(gang.getId(), (GSqlGang) gang);
        nameMap.put(gang.getName(), (GSqlGang) gang);
    }

    @Override
    public void decache(GGang gang) {
        uuidMap.remove(gang.getId());
        nameMap.remove(gang.getName());
    }

    @SneakyThrows
    private GGang decode(ResultSet r){
        UUID id = UUIDUtils.fromBytes(r.getBytes("id"));
        GSqlGang ret = uuidMap.get(id);
        if(ret != null){
            return ret;
        }
        ret = new GSqlGang();
        ret.id = id;
        ret.name = r.getString("name");
        ret.description = r.getString("desc");
        ret.kills = r.getInt("kills");
        ret.deaths = r.getInt("deaths");
        ret.reputation = r.getDouble("reputation");
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select * from safehouses where id =?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            try(ResultSet set = statement.executeQuery()){
                if(set.next()){
                    Location a = new Location(Bukkit.getWorld(UUIDUtils.fromBytes(set.getBytes("world"))),set.getDouble("x"),set.getDouble("y"),set.getDouble("z"),set.getFloat("yaw"),set.getFloat("pitch"));
                    ret.safehouse = a;
                }
                else
                {
                    ret.safehouse = null;
                }
            }
            statement = conn.prepareStatement("select gang_relationships.target, gang_relationships.relationship from gang_relationships where me = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            ret.relationshipMap = new HashMap<>();
            try(ResultSet set = statement.executeQuery()){
                while(set.next()){
                    ret.relationshipMap.put(UUIDUtils.fromBytes(set.getBytes("target")), GangRelationship.fromId(set.getInt("relationship")));
                }
            }
            statement = conn.prepareStatement("select id from players where gang = ?;");
            statement.setBytes(1,UUIDUtils.toBytes(id));
            ret.memberCache = new HashSet<>();
            try(ResultSet set = statement.executeQuery()){
                while(set.next()){
                    ret.memberCache.add(UUIDUtils.fromBytes(set.getBytes("id")));
                }
            }
        }
        return ret;
    }
}
