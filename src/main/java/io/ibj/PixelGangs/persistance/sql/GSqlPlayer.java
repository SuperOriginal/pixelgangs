package io.ibj.PixelGangs.persistance.sql;

import com.google.common.collect.ImmutableSet;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.utils.UUIDUtils;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.NametagManager;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import io.ibj.PixelGangs.persistance.GangRole;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by joe on 12/3/14.
 */
public class GSqlPlayer implements GPlayer {

    @Getter UUID id;
    @Getter String name;
    String displayName;
    @Getter Integer kills;
    @Getter Integer deaths;
    @Getter Double reputation;
    UUID gangId;
    @Getter GangRole role;
    boolean friendlyFire;
    boolean tagsEnabled;

    public String getDisplayName(){
        /*Player p = Bukkit.getPlayer(id);
        if(p != null){
            return p.getDisplayName();
        }
        return displayName;*/
        return getName();
    }

    @Override
    @SneakyThrows
    public void incKills() {
        kills ++;
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("update players set kills = kills +1 where id = ?;");
            statement.setBytes(1, getIdBytes());
            statement.execute();
        }
    }

    @Override
    @SneakyThrows
    public void incDeaths() {
        deaths ++;
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("update players set deaths = deaths +1 where id = ?;");
            statement.setBytes(1, getIdBytes());
            statement.execute();
        }
    }

    @Override
    @SneakyThrows
    public void setReputation(Double reputation) {
        this.reputation = reputation;
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("update players set reputation =? where id =?;");
            statement.setDouble(1,reputation);
            statement.setBytes(2, getIdBytes());
            statement.execute();
        }
    }

    @Override
    public GGang getGang() {
        if(gangId == null){
            return null;
        }
        return PixelGangs.getI().getGangs().getById(gangId);
    }

    @Override
    @SneakyThrows
    public void setRole(GangRole role) {
        if(gangId == null){
            throw new PlayerException(PixelGangs.getI().getFormat("player.role.notPartOfGang","<player>",getDisplayName()));
        }
        this.role = role;
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("update players set role = ? where id = ?;");
            statement.setInt(1,role.getRelation());
            statement.setBytes(2, getIdBytes());
            statement.execute();
        }
    }

    @Override
    public void setRole(GangRole role, GPlayer executor) {
        setRole(role);
        String roleString = "";
        switch(role){
            case MEMBER:
                roleString = "Member";
                break;
            case MVP:
                roleString = "MVP";
                break;
            case LEADER:
                roleString = "Leader";
                break;
        }
        getGang().sendMessageToGang(PixelGangs.getI().getF("player.role.updateRole").replace("<exe>",executor.getDisplayName()).replace("<player>",getDisplayName()).replace("<role>",roleString));
    }

    @Override
    @SneakyThrows
    public Set<GGang> getInvitations() {
        if(gangId != null){
            return ImmutableSet.of();
        }
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("select * from gang_invitations where player = ?;");
            statement.setBytes(1, getIdBytes());
            try(ResultSet set = statement.executeQuery()){
                ImmutableSet.Builder<GGang> builder = ImmutableSet.builder();
                while(set.next()){
                    builder.add(PixelGangs.getI().getGangs().getById(UUIDUtils.fromBytes(set.getBytes("gang"))));
                }
                return builder.build();
            }
        }
    }

    @Override
    public boolean getFriendlyFire() {
        return friendlyFire;
    }

    @Override
    @SneakyThrows
    public void setFriendlyFire(boolean enabled) {
        friendlyFire = enabled;
        try (Connection conn = PixelGangs.getI().getDb()) {
            PreparedStatement statement = conn.prepareStatement("update players set friendlyFire = ? where id = ?;");
            statement.setBoolean(1, enabled);
            statement.setBytes(2, getIdBytes());
            statement.execute();
        }
        Player player = Bukkit.getPlayer(id);
        if (player != null) {
            if (enabled) {
                PixelGangs.getI().getF("player.friendlyFire.enabled").sendTo(player);
            } else {
                PixelGangs.getI().getF("player.friendlyFire.disabled").sendTo(player);
            }
        }
    }

    @Override
    public boolean getTagsEnabled() {
        return tagsEnabled;
    }

    @Override
    @SneakyThrows
    public void setTagsEnabled(boolean enabled) {
        tagsEnabled = enabled;
        try(Connection conn = PixelGangs.getI().getDb()){
            PreparedStatement statement = conn.prepareStatement("update players set tagsEnabled = ? where id = ?;");
            statement.setBoolean(1,enabled);
            statement.setBytes(2, getIdBytes());
            statement.execute();
        }

        updateTheirTags();
    }

    @Override
    public void updateTheirTags() {
        Player me = Bukkit.getPlayer(id);
        if(me == null){
            return;
        }
        if(getTagsEnabled()){
            GGang myGang = getGang();
            for(Player player : Bukkit.getOnlinePlayers()){
                GPlayer gPlayer = PixelGangs.getI().getPlayers().fromPlayer(player);
                GGang gang = gPlayer.getGang();
                if(gang != null){
                    String prefix;
                    String gangTag = gang.getTagName();
                    if(myGang == null){
                        prefix = PixelGangs.getI().getNeutralTag().replaceAll("<gang>", gangTag);
                    }
                    else if(myGang.isAlly(gang)){
                        prefix = PixelGangs.getI().getAllyTag().replaceAll("<gang>", gangTag);
                    }
                    else if(myGang.isEnemy(gang)){
                        prefix = PixelGangs.getI().getEnemyTag().replaceAll("<gang>", gangTag);
                    }
                    else
                    {
                        prefix = PixelGangs.getI().getNeutralTag().replaceAll("<gang>", gangTag);
                    }
                    PixelCore.getI().getPlayerManager().getPlayer(player).getNametagManager().setPrefix(prefix,me);
                }
            }
        }
        else
        {
            for(Player player : Bukkit.getOnlinePlayers()){
                PixelCore.getI().getPlayerManager().getPlayer(player).getNametagManager().setPrefix(null,me);
            }
        }
    }

    @Override
    public void updateMyTag() {
        Player me = Bukkit.getPlayer(id);
        if(me == null){
            return;
        }
        NametagManager myManager = PixelCore.getI().getPlayerManager().getPlayer(me).getNametagManager();
        if(myManager == null){
            return;
        }
        if(getGang() == null){
            for(Player p : Bukkit.getOnlinePlayers()){
                myManager.setPrefix(null,p);
            }
        }
        else {
            for (Player player : Bukkit.getOnlinePlayers()) {
                GPlayer gPlayer = PixelGangs.getI().getPlayers().fromPlayer(player);
                if(gPlayer.getTagsEnabled()){
                    GGang gang = gPlayer.getGang();
                    String prefix;
                    String gangTag = getGang().getTagName();
                    if(gang == null){
                        prefix = PixelGangs.getI().getNeutralTag().replaceAll("<gang>", gangTag);
                    }
                    else if(gang.isAlly(getGang())){
                        prefix = PixelGangs.getI().getAllyTag().replaceAll("<gang>", gangTag);
                    }
                    else if(gang.isEnemy(getGang())){
                        prefix = PixelGangs.getI().getEnemyTag().replaceAll("<gang>", gangTag);
                    }
                    else{
                        prefix = PixelGangs.getI().getNeutralTag().replaceAll("<gang>", gangTag);
                    }
                    myManager.setPrefix(prefix,player);
                }
                else
                {
                    myManager.setPrefix(null,player);
                }
            }
        }
    }

    private byte[] getIdBytes() {
        return UUIDUtils.toBytes(id);
    }
}
