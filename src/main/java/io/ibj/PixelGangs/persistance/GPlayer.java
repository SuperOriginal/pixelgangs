package io.ibj.PixelGangs.persistance;

import java.util.Set;
import java.util.UUID;

/**
 * Created by joe on 11/30/14.
 */
public interface GPlayer {

    UUID getId();
    String getName();
    String getDisplayName();
    Integer getKills();
    void incKills();
    Integer getDeaths();
    void incDeaths();
    Double getReputation();
    void setReputation(Double reputation);
    GGang getGang();
    GangRole getRole();
    void setRole(GangRole role);
    void setRole(GangRole role, GPlayer executor);
    Set<GGang> getInvitations();
    boolean getFriendlyFire();
    void setFriendlyFire(boolean enabled);
    boolean getTagsEnabled();
    void setTagsEnabled(boolean enabled);
    void updateTheirTags();
    void updateMyTag();
}
