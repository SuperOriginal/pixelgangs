package io.ibj.PixelGangs.persistance;

import lombok.Getter;

/**
 * Created by joe on 11/30/14.
 */
public enum GangRole {

    MEMBER(0),
    MVP(1),
    LEADER(2);

    GangRole(int sqlRelation){
        this.relation = sqlRelation;
    }
    @Getter
    int relation;

    public static GangRole fromId(int i){
        switch(i){
            case 0:
                return MEMBER;
            case 1:
                return MVP;
            case 2:
                return LEADER;
            default:
                return null;
        }
    }

}
