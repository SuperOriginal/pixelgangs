package io.ibj.PixelGangs.persistance;

/**
 * Created by joe on 12/1/14.
 */
public enum GangRelationship {

    ALLY(1), //Represents 2 allied gangs
    ENEMY(2),  //Represents me being enemied by target. Me cannot un-enemy.
    ENEMY_SUPER(3),    //Represents me enemying target. I may undo this action.
    ALLY_REQUEST_SENT(4),  //Represents me sending an ally request to target
    ALLY_REQUEST_RECIEVED(5);   //Represents me recieving an ally request from target

    GangRelationship(int id){
        this.id = id;
    }

    public int id;

    public static GangRelationship fromId(int id){
        switch(id){
            case 1:
                return ALLY;
            case 2:
                return ENEMY;
            case 3:
                return ENEMY_SUPER;
            case 4:
                return ALLY_REQUEST_SENT;
            case 5:
                return ALLY_REQUEST_RECIEVED;
            default:
                return null;
        }
    }

}
