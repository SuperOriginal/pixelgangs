package io.ibj.PixelGangs.persistance;

import io.ibj.JLib.format.Format;
import org.bukkit.Location;

import java.util.Set;
import java.util.UUID;

/**
 * Created by joe on 11/30/14.
 */
public interface GGang {

    UUID getId();
    String getName();
    String getTagName();
    void setName(String name, GPlayer executor);
    String getDescription();
    void setDescription(String desc, GPlayer executor);
    Integer getKills();
    void incKills();
    Integer getDeaths();
    void incDeaths();
    Double getReputation();
    void setReputation(Double reputation);
    Set<GPlayer> getMembers();
    boolean isMember(UUID member);
    Set<GGang> getEnemies();
    Set<GGang> getAllies();
    Set<GGang> getAllySentRequests();
    Set<GGang> getAllyRecievedRequests();
    void enemy(GGang gang, GPlayer executor);
    void neutral(GGang gang, GPlayer executor);
    boolean isEnemy(GGang gang);
    boolean isAlly(GGang gang);
    void allyInvite(GGang gang, GPlayer executor);
    void acceptAlly(GGang gang, GPlayer executor);
    Location getSafehouse();
    void setSafehouse(Location l, GPlayer executor);
    void disband(GPlayer executor);
    void disband();
    Set<GPlayer> getInvitations();
    void invite(GPlayer player, GPlayer executor);
    void deinvite(GPlayer player, GPlayer executor);
    void join(GPlayer player);
    void kick(GPlayer player, GPlayer executor);
    void leave(GPlayer player);
    void sendMessageToGang(String message);
    void sendMessageToGang(Format format);
    int getCurrentRank();

}
