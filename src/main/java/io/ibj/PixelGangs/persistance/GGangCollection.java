package io.ibj.PixelGangs.persistance;

import java.util.List;
import java.util.UUID;

/**
 * Created by joe on 11/30/14.
 */
public interface GGangCollection {

    GGang create(String name, GPlayer leader);
    GGang getByName(String name);
    GGang getById(UUID id);
    List<GGang> getLeaderboard();

    void cache(GGang gang);
    void decache(GGang gang);

}
