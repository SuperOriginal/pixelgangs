package io.ibj.PixelGangs.cmd;

import com.earth2me.essentials.utils.StringUtil;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by joe on 12/7/14.
 */
@ForcePlayer
@RootName("join")
@Usage("/crews join [crew]")
@ArgsRange(minimum = 0, maximum = 1)
@Perm("crews.join")
@Desc("Join a gang.")
public class CrewJoinCmd implements ICmd {
    @Override
    public boolean execute(final CommandSender commandSender, final ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer me = PixelGangs.getI().getPlayers().fromPlayer(((Player) commandSender));
                if(argsSet.size() == 0){
                    Set<String> invitations = new HashSet<>();
                    for(GGang g : me.getInvitations()){
                        invitations.add(g.getName());
                    }
                    PixelGangs.getI().getF("gang.join.list").replace("<gangs>",invitations.isEmpty()?"None": StringUtil.joinList(",",invitations)).sendTo(commandSender);
                }
                else
                {
                    GGang target = PixelGangs.getI().getGangs().getByName(argsSet.get(0).getAsString());
                    if(target == null){
                        throw new PlayerException(PixelGangs.getI().getF("gang.join.noGang").replace("<gang>",argsSet.get(0).getAsString()));
                    }
                    target.join(me);
                }
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
