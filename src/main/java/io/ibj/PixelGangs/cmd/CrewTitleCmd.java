package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import io.ibj.PixelGangs.persistance.GangRole;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by joe on 12/7/14.
 */
@ForcePlayer
@RootName("title")
@Usage("/crews title <name>")
@ArgsRange(minimum = 0, maximum = 1)
@Perm("crews.title")
@Desc("Assign a title to a member of your gang.")
public class CrewTitleCmd implements ICmd {
    @Override
    public boolean execute(final CommandSender commandSender, final ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer me = PixelGangs.getI().getPlayers().fromPlayer(((Player) commandSender));
                GGang gang = me.getGang();
                if(gang == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.title.notPartOfAGang"));
                }
                if(argsSet.size() == 0){
                    PixelGangs.getI().getF("gang.title.get").replace("<name>",gang.getName()).sendTo(commandSender);
                    return;
                }
                if(me.getRole()!= GangRole.LEADER){
                    throw new PlayerException(PixelGangs.getI().getF("gang.title.noPerms"));
                }
                gang.setName(argsSet.get(0).getAsString(),me);
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
