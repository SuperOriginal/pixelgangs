package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.CmdWrapper;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.TreeCmd;
import io.ibj.JLib.cmd.annotations.Aliases;
import io.ibj.JLib.cmd.annotations.Desc;
import io.ibj.JLib.cmd.annotations.RootName;
import io.ibj.JLib.cmd.annotations.Usage;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import org.bukkit.command.CommandSender;

/**
 * Created by joe on 12/4/14.
 */
@RootName("crews")
@Aliases({"gangs","gang","crew","c","g"})
@Desc("Root gang command.")
@Usage("/crews <arg>")
public class CrewsCmd extends TreeCmd{

    protected <T extends ICmd> T registerCmd(Class<T> cmd)
    {
        CrewHelpCmd.getCommands().add(CmdWrapper.wrap(cmd));
        return super.registerCmd(cmd);
    }

    public CrewsCmd(){
        registerCmd(CrewAllyCmd.class);
        registerCmd(CrewDeinviteCmd.class);
        registerCmd(CrewDemoteCmd.class);
        registerCmd(CrewDescCmd.class);
        registerCmd(CrewDisbandCmd.class);
        registerCmd(CrewEnemyCmd.class);
        registerCmd(CrewInfoCmd.class);
        registerCmd(CrewInviteCmd.class);
        registerCmd(CrewJoinCmd.class);
        registerCmd(CrewKickCmd.class);
        registerCmd(CrewLeaderboardCmd.class);
        registerCmd(CrewLeaderCmd.class);
        registerCmd(CrewLeaveCmd.class);
        registerCmd(CrewNeutralCmd.class);
        registerCmd(CrewPromoteCmd.class);
        registerCmd(CrewPvpCmd.class);
        registerCmd(CrewRepCmd.class);
        registerCmd(CrewSafehouseCmd.class);
        registerCmd(CrewCreateCmd.class);
        registerCmd(CrewTitleCmd.class);
        registerCmd(CrewHelpCmd.class);
        registerCmd(CrewChatCmd.class);
    }

    @Override
    public void help(CommandSender sender)
    {
        PixelGangs.getI().getF("unknownCommand").sendTo(new CommandSender[] { sender });
    }
}
