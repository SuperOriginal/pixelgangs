package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.utils.StringUtils;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import io.ibj.PixelGangs.persistance.GangRole;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by joe on 12/7/14.
 */
@RootName("info")
@Aliases("i")
@Usage("/crews info [name]")
@ArgsRange(minimum = 0, maximum = 1)
@Perm("crews.info")
@Desc("Find info about a gang.")
public class CrewInfoCmd implements ICmd {
    @Override
    public boolean execute(final CommandSender commandSender, final ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GGang target;
                if(argsSet.size() == 0){
                    if(!(commandSender instanceof Player)){
                        throw new PlayerException("You need to either be a player or specify a gang.");
                    }
                    GPlayer player = PixelGangs.getI().getPlayers().fromPlayer(((Player) commandSender));
                    target = player.getGang();
                    if(target == null){
                        throw new PlayerException(PixelGangs.getI().getF("gang.info.notPartOfAGang"));
                    }
                }
                else
                {
                    target = PixelGangs.getI().getGangs().getByName(argsSet.get(0).getAsString());
                    if(target == null){
                        throw new PlayerException(PixelGangs.getI().getF("gang.info.noGangByThatName").replace("<name>",argsSet.get(0).getAsString()));
                    }
                }

                GPlayer leader = null;
                Set<GPlayer> mvps = new HashSet<>();
                Set<GPlayer> members = new HashSet<>();
                for(GPlayer p : target.getMembers()){
                    if(p.getRole() == GangRole.LEADER){
                        leader = p;
                    }
                    else if(p.getRole() == GangRole.MVP){
                        mvps.add(p);
                    }
                    else
                    {
                        members.add(p);
                    }
                }

                if(leader == null){
                    throw new PlayerException("This gang is invalid. Please report to administrator immediately!");
                }

                Set<String> mvpNames = new HashSet<>();
                for(GPlayer p : mvps){
                    mvpNames.add(p.getDisplayName());
                }
                Set<String> memberNames = new HashSet<>();
                for(GPlayer p : members){
                    memberNames.add(p.getDisplayName());
                }
                Set<String> allies = new HashSet<>();
                for(GGang gang : target.getAllies()){
                    allies.add(gang.getName());
                }

                Set<String> enemies = new HashSet<>();
                for(GGang gang : target.getEnemies()){
                    enemies.add(gang.getName());
                }

                PixelGangs.getI().getF("gang.info.info")
                        .replace("<name>", target.getName())
                        .replace("<desc>",target.getDescription())
                        .replace("<ranking>",target.getCurrentRank())
                        .replace("<reputation>",target.getReputation())
                        .replace("<leader>",leader.getDisplayName())
                        .replace("<mvps>", mvpNames.isEmpty() ? "None":StringUtils.joinList(", ",mvpNames))
                        .replace("<members>", memberNames.isEmpty() ? "None" : StringUtils.joinList(", ",memberNames))
                        .replace("<allies>", allies.isEmpty() ? "None" : StringUtils.joinList(", ", allies))
                        .replace("<enemies>", enemies.isEmpty() ? "None" : StringUtils.joinList(", ", enemies))
                        .sendTo(commandSender);

            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
