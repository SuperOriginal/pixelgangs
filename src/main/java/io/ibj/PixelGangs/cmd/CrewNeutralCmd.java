package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import io.ibj.PixelGangs.persistance.GangRole;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by joe on 12/7/14.
 */
@ForcePlayer
@RootName("neutral")
@Usage("/crews neutral <gang>")
@ArgsEquals(1)
@Perm("crews.neutral")
@Desc("Neutral a gang.")
public class CrewNeutralCmd implements ICmd {
    @Override
    public boolean execute(final CommandSender commandSender, final ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer me = PixelGangs.getI().getPlayers().fromPlayer(((Player) commandSender));
                GGang gang = me.getGang();
                if(gang == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.neutral.notPartOfAGang"));
                }
                if(me.getRole() == GangRole.MEMBER){
                    throw new PlayerException(PixelGangs.getI().getF("gang.neutral.noPerms"));
                }
                GGang target = PixelGangs.getI().getGangs().getByName(argsSet.get(0).getAsString());
                if(target == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.neutral.gangDoesNotExist").replace("<gang>",argsSet.get(0).getAsString()));
                }
                if(target == gang){
                    throw new PlayerException(PixelGangs.getI().getF("gang.neutral.self"));
                }
                gang.neutral(target,me);
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
