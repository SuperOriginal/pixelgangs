package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.utils.StringUtils;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import io.ibj.PixelGangs.persistance.GangRole;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by joe on 12/7/14.
 */
@ForcePlayer
@RootName("invite")
@Aliases("inv")
@Usage("/crews invite [player]")
@ArgsRange(minimum = 0, maximum = 1)
@Perm("crews.invite")
@Desc("Invite a player to your gang.")
public class CrewInviteCmd implements ICmd {
    @Override
    public boolean execute(final CommandSender commandSender, final ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer me = PixelGangs.getI().getPlayers().fromPlayer((Player) commandSender);
                GGang gang = me.getGang();
                if(gang == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.invite.noGang"));
                }
                if(argsSet.size() == 1) {
                    if (me.getRole() == GangRole.MEMBER) {
                        throw new PlayerException(PixelGangs.getI().getF("gang.invite.noPerms"));
                    }
                    GPlayer target = PixelGangs.getI().getPlayers().fromName(argsSet.get(0).getAsString());
                    if (target == null) {
                        throw new PlayerException(PixelGangs.getI().getF("gang.invite.playerNotFound").replace("<player>", argsSet.get(0).getAsString()));
                    }
                    gang.invite(target, me);
                }
                else
                {
                    Set<String> invitations = new HashSet<>();
                    for(GPlayer p : gang.getInvitations()){
                        invitations.add(p.getDisplayName());
                    }
                    if (invitations.isEmpty()){
                        PixelGangs.getI().getF("gang.invite.list").replace("<invitations>","None").sendTo(commandSender);
                    }
                    else
                    {
                        PixelGangs.getI().getF("gang.invite.list").replace("<invitations>", StringUtils.joinList(",",invitations)).sendTo(commandSender);
                    }
                }
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
