package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GPlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by joe on 12/7/14.
 */
@ForcePlayer
@RootName("create")
@Usage("/crews create <name>")
@ArgsEquals(1)
@Perm("crews.create")
@Desc("Create a gang.")
public class CrewCreateCmd implements ICmd {
    @Override
    public boolean execute(CommandSender commandSender, final ArgsSet argsSet) throws PlayerException {
        final Player p = ((Player) commandSender);
        final GPlayer me = PixelGangs.getI().getPlayers().fromPlayer(p); //Cached
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                PixelGangs.getI().getGangs().create(argsSet.get(0).getAsString(),me);
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
