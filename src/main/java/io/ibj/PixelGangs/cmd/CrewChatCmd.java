package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.Executor;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Aaron.
 */
@Cmd(name = "chat", aliases = "c", description = "Toggle ally only chat",usage = "/crews chat <c/a/p>", max = 1, perm = "crews.chat",executors = Executor.PLAYER)
public class CrewChatCmd implements ICmd{
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        Player p = (Player) sender;
        if(args.size() == 0){
            cycleChatType(p.getUniqueId());
            PixelGangs.getI().getF("gang.chat.switchedChatMode").replace("<type>",getChatType(p.getUniqueId()).toString().toLowerCase()).sendTo(p);
            return true;
        }

        String mode = args.get(0).getAsString();
        ChatType req = null;
        if(mode.equalsIgnoreCase("public") || mode.equalsIgnoreCase("p")) req = ChatType.PUBLIC;
        if(mode.equalsIgnoreCase("ally") || mode.equalsIgnoreCase("a")) req = ChatType.ALLY;
        if(mode.equalsIgnoreCase("crew") || mode.equalsIgnoreCase("c") || mode.equalsIgnoreCase("gang") || mode.equalsIgnoreCase("g")) req = ChatType.CREW;

        if (req != null) {
            setChatType(p.getUniqueId(),req);
            PixelGangs.getI().getF("gang.chat.switchedChatMode").replace("<type>", getChatType(p.getUniqueId()).toString().toLowerCase()).sendTo(p);
            return true;
        }else{
            return false;
        }
    }

    public enum ChatType{
        PUBLIC,
        ALLY,
        CREW
    }

    public static ChatType getChatType(UUID uuid){
        ChatType type = PixelGangs.getI().getChatModes().get(uuid);
        if(type == null) return ChatType.PUBLIC;

        return type;
    }

    public void cycleChatType(UUID uuid){
        ChatType type = getChatType(uuid);
        if(PixelGangs.getI().getPlayers().fromID(uuid).getGang() == null){
            PixelGangs.getI().getF("gang.chat.notInCrew").sendTo(uuid);
            PixelGangs.getI().getChatModes().remove(uuid);
            return;
        }
        if(type == ChatType.PUBLIC){
            PixelGangs.getI().getChatModes().put(uuid,ChatType.ALLY);
        }

        if(type == ChatType.ALLY  && PixelGangs.getI().getPlayers().fromID(uuid).getGang() != null){
            PixelGangs.getI().getChatModes().put(uuid,ChatType.CREW);
        }

        if(type == ChatType.CREW){
            PixelGangs.getI().getChatModes().remove(uuid);
        }
    }

    public void setChatType(UUID uuid, ChatType type){
        if(type != ChatType.PUBLIC && PixelGangs.getI().getPlayers().fromID(uuid).getGang() == null){
            PixelGangs.getI().getF("gang.chat.notInCrew").sendTo(uuid);
            PixelGangs.getI().getChatModes().remove(uuid);
            return;
        }
        PixelGangs.getI().getChatModes().put(uuid,type);
    }
}
