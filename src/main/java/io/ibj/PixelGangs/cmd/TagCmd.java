package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.Executor;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.cmd.annotations.Desc;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GPlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author joe 3/1/2015
 */
@Cmd(name = "tag",
description= "Turns on and off crew tags above players heads.",
usage = "/tag [on/off]",
max = 1,
executors = Executor.PLAYER)
@Desc("Turns on and off crew tags above players heads.")
public class TagCmd implements ICmd {
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        boolean newSetting;
        GPlayer player = PixelGangs.getI().getPlayers().fromPlayer((Player) sender);
        if(args.size() == 0){
            newSetting = !player.getTagsEnabled();
        }
        else
        {
            String arg = args.get(0).getAsString();
            if(arg.equalsIgnoreCase("on")){
                newSetting = true;
            }
            else if (arg.equalsIgnoreCase("off")) {
                newSetting = false;
            }
            else
            {
                return false;
            }
        }
        player.setTagsEnabled(newSetting);
        if(newSetting){
            PixelGangs.getI().getF("tags.enable").sendTo(sender);
        }
        else
        {
            PixelGangs.getI().getF("tags.disable").sendTo(sender);
        }
        return true;
    }
}
