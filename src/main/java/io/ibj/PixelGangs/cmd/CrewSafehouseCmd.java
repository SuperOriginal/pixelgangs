package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.JLib;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.TreeCmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by joe on 12/7/14.
 */
@ForcePlayer
@RootName("safehouse")
@Usage("/crews safehouse")
@Perm("crews.safehouse")
@Desc("Teleport to your gang's safehouse.")
public class CrewSafehouseCmd extends TreeCmd {

    public CrewSafehouseCmd(){
        registerCmd(CrewSafehouseSetCmd.class);
        registerCmd(CrewSafehouseUnsetCmd.class);
    }

    @Override
    public boolean executeIfNoSubFound(final CommandSender sender, ArgsSet args) throws PlayerException {
        if(args.size() != 0){
            return false;
        }
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer me = PixelGangs.getI().getPlayers().fromPlayer(((Player) sender));
                GGang gang = me.getGang();
                if(gang == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.safehouse.notPartOfAGang"));
                }
                final Location safehouse = gang.getSafehouse();
                if(safehouse == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.safehouse.safehouseNotSet"));
                }
                PixelGangs.getI().runSync(new Runnable() {
                    @Override
                    public void run() {
                        JLib.getI().tp(((Player) sender),safehouse);
                    }
                });
            }
        },sender, ThreadLevel.ASYNC);
        return true;
    }
}
