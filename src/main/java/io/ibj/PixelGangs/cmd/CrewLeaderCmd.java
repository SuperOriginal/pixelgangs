package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import io.ibj.PixelGangs.persistance.GangRole;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by joe on 12/7/14.
 */
@ForcePlayer
@RootName("leader")
@Usage("/crews leader <player>")
@ArgsEquals(1)
@Perm("crews.leader")
@Desc("Give a player leader.")
public class CrewLeaderCmd implements ICmd {
    @Override
    public boolean execute(final CommandSender commandSender, final ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer me = PixelGangs.getI().getPlayers().fromPlayer(((Player) commandSender));
                GGang gang = me.getGang();
                if(gang == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.leader.notPartOfAGang"));
                }
                if(me.getRole() != GangRole.LEADER){
                    throw new PlayerException(PixelGangs.getI().getF("gang.leader.noPerms"));
                }
                GPlayer target = PixelGangs.getI().getPlayers().fromName(argsSet.get(0).getAsString());
                if(target == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.leader.hasNotPlayedBefore").replace("<player>",argsSet.get(0).getAsString()));
                }
                if(target.getGang() != gang){
                    throw new PlayerException(PixelGangs.getI().getF("gang.leader.notPartOfThisGang").replace("<player>",target.getDisplayName()));
                }
                if(target == me){
                    throw new PlayerException(PixelGangs.getI().getF("gang.leader.self"));
                }
                target.setRole(GangRole.LEADER,me);
                me.setRole(GangRole.MEMBER);
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
