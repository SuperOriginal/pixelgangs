package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

/**
 * Created by joe on 12/7/14.
 */
@ForcePlayer
@RootName("rep")
@Usage("/crews rep")
@ArgsRange(minimum = 0, maximum = 1)
@Perm("crews.reputation")
@Desc("View a player's reputation.")
public class CrewRepCmd implements ICmd{
    DecimalFormat format = new DecimalFormat("0.00");
    @Override
    public boolean execute(final CommandSender commandSender, final ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer target;
                if(argsSet.size() == 0){
                    target = PixelGangs.getI().getPlayers().fromPlayer(((Player) commandSender));
                }
                else
                {
                    target = PixelGangs.getI().getPlayers().fromName(argsSet.get(0).getAsString());
                    if(target == null){
                        throw new PlayerException(PixelGangs.getI().getF("rep.notPlayedBefore").replace("<name>", argsSet.get(0).getAsString()));
                    }
                }
                GGang gang = target.getGang();
                PixelGangs.getI().getF("rep.output").replace("<self>",target.getReputation()).replace("<gang>",gang==null?"Not a part of crew":gang.getReputation())
                        .replace("<kills>",target.getKills()).replace("<deaths>",target.getDeaths()).replace("<kdr>",format.format((double)target.getKills() / (double)target.getDeaths())).sendTo(commandSender);
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
