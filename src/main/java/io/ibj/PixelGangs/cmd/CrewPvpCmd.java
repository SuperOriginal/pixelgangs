package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GPlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by joe on 12/7/14.
 */
@RootName("pvp")
@Usage("/crews pvp")
@ArgsEquals(0)
@Perm("crews.pvp")
@Desc("Toggle friendly fire.")
public class CrewPvpCmd implements ICmd {
    @Override
    public boolean execute(final CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer me = PixelGangs.getI().getPlayers().fromPlayer(((Player) commandSender));
                me.setFriendlyFire(!me.getFriendlyFire());
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
