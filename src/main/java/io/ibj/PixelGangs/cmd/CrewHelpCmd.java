package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.CmdWrapper;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Aliases;
import io.ibj.JLib.cmd.annotations.ArgsRange;
import io.ibj.JLib.cmd.annotations.Desc;
import io.ibj.JLib.cmd.annotations.Perm;
import io.ibj.JLib.cmd.annotations.RootName;
import io.ibj.JLib.cmd.annotations.Usage;
import io.ibj.JLib.cmd.args.Arg;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.format.Format;
import io.ibj.JLib.format.TagStyle;
import io.ibj.PixelGangs.PixelGangs;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.CommandSender;

@RootName("help")
@Usage("/crews help")
@ArgsRange(minimum=0, maximum=999)
@Perm("crews.leave")
@Desc("Help command for Gangs.")
@Aliases({"h"})
public class CrewHelpCmd
        implements ICmd
{
    private static List<CmdWrapper> commands = new ArrayList();

    public boolean execute(CommandSender sender, ArgsSet args)
            throws PlayerException
    {
        int page = 1;
        if (args.size() > 0) {
            try
            {
                page = Integer.parseInt(((Arg)args.get(0)).getAsString());
                if (page <= 0) {
                    page = 1;
                }
            }
            catch (NumberFormatException localNumberFormatException) {}
        }
        page(sender, commands, page);
        return true;
    }

    public void page(CommandSender sender, List<CmdWrapper> data, int page)
    {
        int pageSize = PixelGangs.getI().getGangHelpPageSize();
        if (page > data.size() / pageSize + 1) {
            page = data.size() / pageSize + 1;
        }
        int startEntryIndex = page == 1 ? 0 : (page - 1) * pageSize;
        if (data.isEmpty()) {
            return;
        }
        int maxPage = data.size() / pageSize + 1;

        Format header = PixelGangs.getI().getF("help.header").replace("<maxPage>", Integer.valueOf(maxPage)).replace("<currentPage>", Integer.valueOf(page)).setTagStyle(TagStyle.HIDDEN);
        header.sendTo(new CommandSender[] { sender });
        for (int entryIndex = startEntryIndex; entryIndex < pageSize + startEntryIndex; entryIndex++) {
            if ((entryIndex < data.size()) && (data.get(entryIndex) != null)) {
                PixelGangs.getI().getF("help.body").replace("<usage>", ((CmdWrapper)data.get(entryIndex)).getUsage()).replace("<description>", ((CmdWrapper)data.get(entryIndex)).getDescription()).setTagStyle(TagStyle.HIDDEN).sendTo(new CommandSender[] { sender });
            }
        }
    }

    public static List<CmdWrapper> getCommands()
    {
        return commands;
    }
}
