package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import io.ibj.PixelGangs.persistance.GangRole;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by joe on 12/7/14.
 */
@ForcePlayer
@RootName("enemy")
@Usage("/crews enemy <gang>")
@ArgsEquals(1)
@Perm("crews.enemy")
@Desc("Enemy a gang.")
public class CrewEnemyCmd implements ICmd {
    @Override
    public boolean execute(final CommandSender commandSender, final ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer me = PixelGangs.getI().getPlayers().fromPlayer(((Player) commandSender));
                GGang gang = me.getGang();
                if(gang == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.enemy.notPartOfAGang"));
                }
                if(me.getRole() == GangRole.MEMBER){
                    throw new PlayerException(PixelGangs.getI().getF("gang.enemy.noPerms"));
                }
                GGang target = PixelGangs.getI().getGangs().getByName(argsSet.get(0).getAsString());
                if(target == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.enemy.gangDoesNotExist").replace("<gang>",argsSet.get(0).getAsString()));
                }
                if(target == gang){
                    throw new PlayerException(PixelGangs.getI().getF("gang.enemy.self"));
                }
                gang.enemy(target,me);
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
