package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import io.ibj.PixelGangs.persistance.GPlayer;
import io.ibj.PixelGangs.persistance.GangRole;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by joe on 12/7/14.
 */
@ForcePlayer
@RootName("unset")
@Usage("/crews safehouse unset")
@ArgsEquals(0)
@Perm("crews.safehouse.unset")
@Desc("Unset your crew's safehouse location.")
public class CrewSafehouseUnsetCmd implements ICmd {
    @Override
    public boolean execute(final CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                GPlayer me = PixelGangs.getI().getPlayers().fromPlayer(((Player) commandSender));
                GGang gang = me.getGang();
                if(gang == null){
                    throw new PlayerException(PixelGangs.getI().getF("gang.safehouse.noPerms"));
                }
                if(me.getRole() == GangRole.MEMBER){
                    throw new PlayerException(PixelGangs.getI().getF("gang.safehouse.notPartOfAGang"));
                }
                gang.setSafehouse(null,me);
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
