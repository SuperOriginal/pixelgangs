package io.ibj.PixelGangs.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.*;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.format.Format;
import io.ibj.PixelGangs.PixelGangs;
import io.ibj.PixelGangs.persistance.GGang;
import org.bukkit.command.CommandSender;

import java.util.List;

/**
 * Created by joe on 12/7/14.
 */
@RootName("leaderboard")
@Usage("/crews leaderboard")
@Perm("crews.leaderboard")
@Desc("View the gang leaderboard.")
public class CrewLeaderboardCmd implements ICmd {
    @Override
    public boolean execute(final CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        PixelGangs.getI().runNow(new Runnable() {
            @Override
            public void run() {
                List<GGang> leaders = PixelGangs.getI().getGangs().getLeaderboard();
                Format f = PixelGangs.getI().getF("leaderboard");
                for(int i = 1; i<=leaders.size();i++){
                    f.replace("<gang"+i+"name>",leaders.get(i-1).getName());
                    f.replace("<gang"+i+"rep>", leaders.get(i-1).getReputation());
                }
                f.sendTo(commandSender);
            }
        },commandSender, ThreadLevel.ASYNC);
        return true;
    }
}
