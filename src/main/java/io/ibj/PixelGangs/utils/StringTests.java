package io.ibj.PixelGangs.utils;

import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;

/**
 * Created by joe on 12/10/14.
 */
public class StringTests {

    private static final char[] allowedChars = "abcdefghijklmnopqrstuvwxyz123456789+=-_.<?>ABCDEFGHIJKLMNOPQRSTUVWXYZ ".toCharArray();

    public static void testContents(String s){
        for(char c : s.toCharArray()){
            boolean found = false;
            for(char c1 : allowedChars){
                if(c == c1){
                    found = true;
                    break;
                }
            }
            if(!found){
                throw new PlayerException(PixelGangs.getI().getF("illegalChars").replace("<test>",s));
            }
        }
    }

}
