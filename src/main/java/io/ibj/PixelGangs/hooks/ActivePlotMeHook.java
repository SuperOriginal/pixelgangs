package io.ibj.PixelGangs.hooks;


import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelGangs.PixelGangs;
import me.kyle.plotz.api.PlotzAPI;
import me.kyle.plotz.obj.Plot;
import org.bukkit.Location;

import java.util.Set;
import java.util.UUID;

/**
 * Created by joe on 12/2/14.
 */
public class ActivePlotMeHook extends PlotHook {
    @Override
    public void testOwner(Location l, Set<UUID> id) {
        Plot standingOn = PlotzAPI.getPlotByLocation(l);
        if(standingOn == null){
            throw new PlayerException(PixelGangs.getI().getF("gang.safehouse.notInMemberPlot"));
        }
        if(!id.contains(standingOn.getOwner())){
            throw new PlayerException(PixelGangs.getI().getF("gang.safehouse.notInMemberPlot"));
        }
    }

    @Override
    public boolean isHooked() {
        return true;
    }
}
